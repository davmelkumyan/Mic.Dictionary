﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarchy
{
    class City : Dictionary<string, Street>
    {
        public string CityName { get; set; }

        public City(string cityName)
        {
            this.CityName = cityName;
        }

        public void Add(Street st)
        {
            if(!ContainsKey(st.StreetName))
            base.Add(st.StreetName, st);
        }

        public override string ToString()
        {
            return "City " + CityName;
        }
    }
}
