﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarchy
{
    class Street : Dictionary<string, Building>
    {
        public string StreetName { get; set; }

        public Street(string streetName)
        {
            this.StreetName = streetName;
        }

        public void Add(Building b)
        {
            if(!ContainsKey(b.BuildingName))
            base.Add(b.BuildingName, b);
        }

        public override string ToString()
        {
            return StreetName + " Street";
        }
    }
}
