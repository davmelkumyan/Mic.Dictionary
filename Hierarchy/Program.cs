﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarchy
{
    class Program
    {
        static void Main(string[] args)
        {
            var erevan = new City("Erevan");

            var abovyan = new Street("Abovyan");
            var tumanyan = new Street("Tumanyan");
            var amiryan = new Street("Amiryan");
            var vaxarshyan = new Street("Vaxarshyan");

            var first = new Building("1");
            var second = new Building("2");
            var third = new Building("3");
            var forth = new Building("4");
            var fifth = new Building("5");

            abovyan.Add(first);
            abovyan.Add(second);
            abovyan.Add(third);
            abovyan.Add(forth);
            abovyan.Add(fifth);

            tumanyan.Add(first);
            tumanyan.Add(second);
            tumanyan.Add(third);
            tumanyan.Add(forth);
            tumanyan.Add(fifth);

            amiryan.Add(first);
            amiryan.Add(second);
            amiryan.Add(third);
            amiryan.Add(forth);
            amiryan.Add(fifth);

            vaxarshyan.Add(first);
            vaxarshyan.Add(second);
            vaxarshyan.Add(third);
            vaxarshyan.Add(forth);
            vaxarshyan.Add(fifth);

            erevan.Add(abovyan);
            erevan.Add(tumanyan);
            erevan.Add(amiryan);
            erevan.Add(vaxarshyan);

            
            Console.WriteLine(erevan);
            Console.WriteLine(erevan["Vaxarshyan"]);
            Console.WriteLine(erevan["Vaxarshyan"]["1"]);

            foreach (var item in erevan)
            {
                Console.WriteLine(item.Value["1"]);
            }
        }
    }
}
