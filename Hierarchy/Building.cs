﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierarchy
{
    class Building
    {
        public string BuildingName { get; set; }

        public Building(string buildingName)
        {
            this.BuildingName = buildingName;
        }

        public override string ToString()
        {
            return "Build " + BuildingName;
        }
    }
}
